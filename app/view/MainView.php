<?php

namespace app\view;

/**
 * Description of MainView
 *
 * @author Dominic
 */
class MainView extends \rueckgrat\mvc\FastView {
    
    protected $users;
    protected $user;


    public function __construct() {
        parent::__construct();
        $this->cacheDisabled = TRUE;
        $this->title = 'Hello Framework';
    }
    
    public function renderFrontPage($user){
        $this->users = $user;
        $this->pageContent = $this->getCompiledTpl("main/main");
        return $this->renderMainPage();
    }
    
    public function renderEditPage(\app\mapper\User $user) {
        $this->user = $user;
        $this->pageContent = $this->getCompiledTpl("main/edit");
        return $this->renderMAinPage();
                
    }
}
