<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author Dominic
 */
class User extends \rueckgrat\db\Mapper {
    
    protected $prename;
    protected $name;
    protected $mail;


    public function _construct(){
        parent::_construct();
    }
    function getPrename() {
        return $this->prename;
    }

    function getName() {
        return $this->name;
    }

    function getMail() {
        return $this->mail;
    }


}

